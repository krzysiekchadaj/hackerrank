#include <iostream>

using namespace std;

int main()
{
    int N, Q, K, i, j;
    cin >> N >> Q;

    int ** A = new int * [N];
    for (int n = 0; n < N; n++)
    {
        cin >> K;
        
        A[n] = new int[K];
        for(int k = 0; k < K; k++)
            cin >> A[n][k];
    }
    
    for(int q = 0; q < Q; q++)
    {
        cin >> i >> j;
        cout << A[i][j] << endl;
    }

    for(int n = 0; n < N; n++)
        delete[] A[n];
    delete[] A;

	return 0;
}
