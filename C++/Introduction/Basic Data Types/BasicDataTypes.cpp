#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    int a;
    long b;
    long long c;
    char d;
    float e;
    double f;
    
    // C
    // scanf("%d %ld %lld %c %f %lf", &a, &b, &c, &d, &e, &f);
    //printf("%d\n%ld\n%lld\n%c\n%.2f\n%.5lf\n", a, b, c, d, e, f);
      
    // C++
    cin >> a >> b >> c >> d >> e >> f;
    
    cout << a << endl;
    cout << b << endl;
    cout << c << endl;
    cout << d << endl;
    cout << fixed << setprecision(2) << e << endl;
    cout << fixed << setprecision(5) << f;
    
    return 0;
}
