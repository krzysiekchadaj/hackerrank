#include <iostream>
#include <set>

using namespace std;

int main()
{
    int T;
    cin >> T;
    
    set<int> s;
    
    for (int t = 0; t < T; t++)
    {
        int y, x;
        cin >> y >> x;
        
        switch (y)
        {
            case 1:
                s.insert(x);
                break;
            
            case 2:
                s.erase(x);
                break;
            
            case 3:
                set<int>::iterator itr = s.find(x);
                if (itr != s.end())
                    cout << "Yes" << endl;
                else
                    cout << "No" << endl;
                break;
        }
    }
    
    return 0;
}
