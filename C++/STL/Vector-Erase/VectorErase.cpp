#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int main() {
    int n, k, m;
    cin >> n;
    vector<int> l(n);
    for(int i=0; i<n; i++)
        cin >> l[i];
    cin >> k;
    l.erase(l.begin()+k-1);
    
    cin >> k >> m;
    l.erase(l.begin()+k-1, l.begin()+m-1);
    cout << l.size() << endl;
    for(int i=0; i<l.size(); i++)
        cout << l[i] << " ";
    return 0;
}
