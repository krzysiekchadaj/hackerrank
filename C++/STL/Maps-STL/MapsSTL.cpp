#include <iostream>
#include <map>
#include <string>

using namespace std;

int main()
{
    int T;
    cin >> T;
    
    map <string, int> m;
    map <string, int>::iterator itr;
    string name;
    int type, Y;
    
    for(int t = 0; t < T; t++)
    {
        cin >> type;
        switch (type)
        {
            case 1:
                cin >> name >> Y;
                itr = m.find(name);
                if (itr != m.end())
                    m[name] += Y;
                else
                    m.insert(make_pair(name, Y));
                break;
            
            case 2:
                cin >> name;
                m.erase(name);
                break;
            
            case 3:
                cin >> name;
                cout << m[name] << endl;
                break;
        }
    }
    
    return 0;
}
