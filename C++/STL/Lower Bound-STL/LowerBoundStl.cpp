#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

bool myfunction (int i, int j) { return (i<j); }

int main() {
    int n;
    cin >> n;
    vector<int> v(n);
    for (int i=0; i<n; i++)
        cin >> v[i];
    
    int q;
    cin >> q;
    
    
    int s;
    int k;
    vector<int>::iterator it;
    for (int i=0; i<q; i++) {
        cin >> k;
        s = 0;
        it = lower_bound(v.begin(), v.end(), k);
        if ( *it==k )
            cout << "Yes " << it-v.begin()+1 << endl;
        else
            cout << "No " << it-v.begin()+1 << endl;
    }
    return 0;
}
