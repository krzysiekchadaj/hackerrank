#include <sstream>
#include <vector>
#include <iostream>
using namespace std;

vector<int> parseInts(string str) {
    int i;
    char ch(',');
    vector<int> k;
    
    stringstream ss(str);
    while(1) {
        ss >> i;
        k.push_back(i);
        ss >> ch;
        if( ss.eof() )
            break;   
    }
    return k;
}

int main() {
    string str;
    cin >> str;
    vector<int> integers = parseInts(str);
    for(int i = 0; i < integers.size(); i++) {
        cout << integers[i] << "\n";
    }
    
    return 0;
}
