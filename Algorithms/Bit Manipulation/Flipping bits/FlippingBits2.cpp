#include <iostream>

using namespace std;

int main()
{
    int T;
    cin >> T;
    
    unsigned int num;
    for(int t = 0; t < T; t++)
    {
        cin >> num;
        cout << ~num << endl;
    }

    return 0;
}
