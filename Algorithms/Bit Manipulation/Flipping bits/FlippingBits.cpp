#include <iostream>
#include <stdint.h>

using namespace std;

int main()
{
    int T;
    cin >> T;
    
    uint32_t num;
    for(int t = 0; t < T; t++)
    {
        cin >> num;
        cout << ~num << endl;
    }

    return 0;
}
