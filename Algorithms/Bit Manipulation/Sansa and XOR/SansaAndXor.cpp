#include <cmath>
#include <cstdio>
#include <iostream>

using namespace std;

int main() {
    
    int T; // Test cases
    int N; // Number of elements 
    int result;
    int partial_result;
    
    // Read number of test cases
    cin >> T;
    
    for(int t=0; t<T; t++) {
        // Number of data elements
        cin >> N;
        
        // Read elements
        int * A = new int[N];
        for(int i=0; i<N; i++)
            cin >> A[i];
    
        // XOR identity term
        result = 0;
        if( N%2 ) {
            for(int i=0; i<N; i+=2)
                result ^= A[i];    
        }
        
        cout << result << endl;
        delete[] A;
    }
        
    return 0;
}
