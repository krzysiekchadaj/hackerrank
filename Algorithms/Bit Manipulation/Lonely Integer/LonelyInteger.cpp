#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
    int N;
    cin >> N;

    vector<int> a(N);
    for(int i = 0; i < N; i++)
        cin >> a.at(i);
    
    if ( a.size() == 1 ) 
    {
        cout << a.at(0);
        return 0;
    }
    
    sort(a.begin(), a.end());
    
    for(int i = 1; i < N; i+=2)
    {
        if( a.at(i) != a.at(i-1) )
        {
            cout << a.at(i-1);
            return 0;
        }
    }
    cout << a.at(N-1);
    
    return 0;
}
