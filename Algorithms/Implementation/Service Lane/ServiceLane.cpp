#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int N, T;
    cin >> N >> T;
    
    vector<int> A(N);
    for(int i = 0; i < N; i++)
        cin >> A[i];
    
    int x, y, min;
    for(int t = 0; t < T; t++)
    {
        cin >> x >> y;
        min = 3;
        for(int i = x; i <= y; i++)
            if ( A[i] < min )
                min = A[i];
        cout << min << endl;
    }
    
    return 0;
}
