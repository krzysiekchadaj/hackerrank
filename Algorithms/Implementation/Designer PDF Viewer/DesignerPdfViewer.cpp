#include <iostream>
#include <vector>
#include <algorithm>

#define n 26

using namespace std;

int main()
{
    vector<int> h(n);
    for(int i = 0; i < n; i++)
       cin >> h[i];

    string word;
    cin >> word;
    
    int width = word.size();

    int height = 0;
    int letterHeight = 0;
    for(int i = 0; i < word.size(); i++)
    {
        letterHeight = h[(int)word.at(i)-'a'];
        if (letterHeight > height)
            height = letterHeight;
    }
     
    cout << width * height;
    return 0;
}
