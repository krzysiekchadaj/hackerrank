#include <iostream>
#include <vector>

using namespace std;

int main()
{
	int chapters, problemsPerPage;
	cin >> chapters >> problemsPerPage;

	vector<int> problems(chapters);
	for (int c = 0; c < chapters; c++)
		cin >> problems.at(c);

	int special = 0;
	int page = 1;
	int problemCount = 0;

	for (int c = 0; c < chapters; c++)
    {
		for (int i = 1; i <= problems.at(c); i++)
		{
			problemCount += 1;
			if (problemCount == problemsPerPage + 1)
			{
				problemCount = 1;
				page += 1;
			}

			if (page == i)
				special += 1;
		}
        
		page += 1;
		problemCount = 0;
	}
	cout << special;

	return 0;
}
