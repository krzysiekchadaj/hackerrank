#include <iostream>

#define YEARLY_FEE 10000
#define MONTHLY_FEE 500
#define DAILY_FEE 15

using namespace std;

struct date
{
    int day;
    int month;
    int year;
};

int LibraryFine(date ret, date exp)
{
    if (ret.year > exp.year)
        return YEARLY_FEE;
    else if (ret.year == exp.year)
        if (ret.month > exp.month)
            return (MONTHLY_FEE*(ret.month-exp.month));
        else if (ret.month == exp.month)
            if (ret.day > exp.day)
                return (DAILY_FEE*(ret.day-exp.day));
            else
                return 0;
        else
            return 0;
    else
        return 0;
}

int main()
{
    date ret, exp;
    cin >> ret.day >> ret.month >> ret.year;
    cin >> exp.day >> exp.month >> exp.year;

    cout << LibraryFine(ret, exp);
    return 0;
}
