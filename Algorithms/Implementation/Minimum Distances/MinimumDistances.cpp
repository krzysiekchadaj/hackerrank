#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

int main()
{
    int n;
    cin >> n;
    
    vector<int> A(n);
    for(int i = 0;i < n;i++)
       cin >> A[i];

    int d = n;
    
    for(int i = 0; i < n; i++)
        for(int j = i+1; j < n; j++)
            if( A[i] == A[j] &&  abs(i - j) < d )
                d = abs(i - j);
    
    if ( d == n)
        d = -1;

    cout << d;
    
    return 0;
}
