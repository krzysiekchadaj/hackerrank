#include <iostream>
#include <vector>

using namespace std;

template<class T>
void print(vector<T> * a, int size)
{
    for(int i = 0; i < size; i++)
        cout << a->at(i) << " ";
    cout << endl;
}

int main()
{
    int citiesCount, stationsCount;
    cin >> citiesCount >> stationsCount;
    
    vector<bool> stations(citiesCount);
    for(int i = 0; i < citiesCount; i++)
        stations.at(i) = false;
    
    int idx;
    for(int i = 0; i < stationsCount; i++)
    {
        cin >> idx;
        stations.at(idx) = true;
    }
    // print(&stations, citiesCount);
    
    
    idx = -1;
    vector<int> distanceLeft(citiesCount);
    for(int i = 0; i < citiesCount; i++)
    {
        if(stations.at(i))
            idx = 0;
        if (idx == -1)
            distanceLeft.at(i) = -1;
        else
            distanceLeft.at(i) = idx++;
    }
    // print(&distanceLeft, citiesCount);
    
    idx = -1;
    vector<int> distanceRight(citiesCount);
    for(int i = citiesCount - 1; i > -1; i--)
    {
        if(stations.at(i))
            idx = 0;
        if (idx == -1)
            distanceRight.at(i) = -1;
        else
            distanceRight.at(i) = idx++;
    }
    // print(&distanceRight, citiesCount);
    
    vector<int> distance(citiesCount);
    for(int i = 0; i < citiesCount; i++)
        distance.at(i) = -1;
    
    bool left, right;
    for(int i = 0; i < citiesCount; i++)
    {
        left  = ( distanceLeft.at(i) == -1 );
        right = ( distanceRight.at(i) == -1 );
        
        if (left && right)
        {
            distance.at(i) = -1;
            continue;
        }
        if (left)
        {
            distance.at(i) = distanceRight.at(i);
            continue;
        }
        if (right)
        {
            distance.at(i) = distanceLeft.at(i);
            continue;
        }
        distance.at(i) = ( distanceRight.at(i) < distanceLeft.at(i) ? distanceRight.at(i) : distanceLeft.at(i) );
    }
    
    int result = 0;
    for(int i = 0; i < citiesCount; i++)
        if ( distance.at(i) != -1 && result < distance.at(i) )
            result = distance.at(i);
    // print(&distance, citiesCount);
        
    cout << result << endl;
    return 0;
}
