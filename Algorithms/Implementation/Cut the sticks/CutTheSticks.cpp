#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
    int size;
    cin >> size;
    
    vector<int> array(size);
    for(int i = 0; i < size; i++)
       cin >> array[i];

    sort(array.begin(), array.end());
    
    cout << size << endl;
    for(int i = 1; i < size; i++)
        if ( array[i] != array[i-1] )
            cout << size - i << endl;
    
    return 0;
}
