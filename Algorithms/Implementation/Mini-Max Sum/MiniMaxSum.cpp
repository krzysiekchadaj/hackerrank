#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>

#define SIZE 5

using namespace std;

int main()
{
    vector<int> A(SIZE);
    for(int i = 0; i < SIZE; i++)
        cin >> A.at(i);
    
    sort(A.begin(), A.end());
    
    cout << accumulate(A.begin(), A.end()-1, (long int) 0);
    cout << " ";
    cout << accumulate(A.begin()+1, A.end(), (long int) 0);
    
    return 0;
}
