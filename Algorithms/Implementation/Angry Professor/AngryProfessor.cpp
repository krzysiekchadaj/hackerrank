#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
    int tests;
    cin >> tests;
    
    for(int test_case = 0; test_case < tests; test_case++)
    {
        int N, K;
        cin >> N >> K;
        
        vector<int> array(N);
        for(int i = 0; i < N; i++)
           cin >> array[i];

        sort(array.begin(), array.end());
        
        int s = 0;
        int i = 0;
        while (array[i] <= 0)
            i++;
        
        if (i < K)
            cout << "YES" << endl;
        else
            cout << "NO" << endl;
    }
    
    return 0;
}
