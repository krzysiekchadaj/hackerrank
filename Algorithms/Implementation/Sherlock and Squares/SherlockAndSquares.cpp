#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    int T;
    cin >> T;
    
    for(int test_case = 0; test_case < T; test_case++)
    {
        int A, B;
        cin >> A >> B;
        
        int sqrtA = ceil(sqrt(A));
        int sqrtB = floor(sqrt(B));
        
        cout << sqrtB - sqrtA + 1 << endl;
    }
    
    return 0;
}
