#include <iostream>

using namespace std;

int main()
{
    int N;
    cin >> N;
    
    int liked = 2;
    int result = 2;
    
    for(int i = 1; i < N; i++)
    {
        liked = (liked * 3) / 2;
        result += liked;
    }
    cout << result;
    
    return 0;
}
