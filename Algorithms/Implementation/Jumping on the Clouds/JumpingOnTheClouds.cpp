#include <iostream>
#include <vector>

using namespace std;

inline int min(int a, int b)
{
	return (a < b ? a : b);
}

struct cloud
{
	bool isJumpImpossible;
	int jumpsCount;
};

int main()
{
	// read inputs
	int numberOfClouds;
	cin >> numberOfClouds;

	// allocate memory 
	vector<cloud> clouds(numberOfClouds);

	// read inputs
	for (int c = 0; c < numberOfClouds; c++)
		cin >> clouds[c].isJumpImpossible;

	// use dynamic programming to compute minimal number of jumps required
	clouds[0].jumpsCount = 0;
	for (int c = 1; c < numberOfClouds; c++)
	{
		clouds[c].jumpsCount = min(
			(c - 2 >= 0 && !clouds[c - 2].isJumpImpossible) ? clouds[c - 2].jumpsCount : numberOfClouds,
			(c - 1 >= 0 && !clouds[c - 1].isJumpImpossible) ? clouds[c - 1].jumpsCount : numberOfClouds) + 1;
	}

	// print result
	cout << clouds[numberOfClouds - 1].jumpsCount;

	return 0;
}
