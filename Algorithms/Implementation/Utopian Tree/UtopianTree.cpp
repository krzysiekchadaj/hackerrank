#include <iostream>

using namespace std;

int main()
{
    int T;
    cin >> T;
    
    int height;
    
    for(int t = 0; t < T; t++)
    {
        int n;
        cin >> n;
        
        height = 1;
        
        for(int i = 1; i < n; i+=2)
            height = 2 * height + 1;
        
        if ( n % 2 == 1 )
            height *= 2;
        
        cout << height << endl;
    }
    return 0;
}
