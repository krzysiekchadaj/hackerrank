#include <iostream>

using namespace std;

int main()
{
    string s;
    cin >> s;
    
    int size = s.size();
    
    long n;
    cin >> n;
    
    // count how many 'a' the string has
    long count = 0;
    for(int i = 0; i < size; i++)
        if ( s.at(i) == 'a' )
            count += 1;
        
    // Compute how many copies of string s is within n characters and multiply the counter
    count *= (n / size);
    
    // Finally, there can be some letters left
    int limit = n - size * (n / size);
    for(int i = 0; i < limit; i++)
        if ( s.at(i) == 'a' )
            count += 1;
    
    cout << count;
    
    return 0;
}
