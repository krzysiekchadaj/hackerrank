#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
    // read input
    int numberOfSocks;
    cin >> numberOfSocks;
    
    // read input
    vector<int> socks(numberOfSocks);
    for(int s = 0; s < numberOfSocks; s++)
       cin >> socks[s];

    // sort input
    sort(socks.begin(), socks.end());
    
    // loop over sorted socks and count pairs
    int pairs = 0;
    int s = 1;
    while(s < numberOfSocks)
    {
        if( socks[s-1] == socks[s] )
        {
            pairs += 1;
            s += 2;
        }
        else
        {
            s += 1;
        }
    }
    
    // print result
    cout << pairs;
    
    return 0;
}
