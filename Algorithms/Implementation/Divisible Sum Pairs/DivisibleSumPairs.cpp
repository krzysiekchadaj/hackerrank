#include <iostream>

using namespace std;

int main()
{
    int n, k;
    cin >> n >> k;
    
    int * a = new int[n];
    for(int i = 0; i < n; i++)
        cin >> a[i];
    
    int result = 0;
    for(int i=0; i<n; i++)
        for(int j=i+1; j<n; j++)
            if ((a[i]+a[j])%k == 0)
                result += 1;
                
    cout << result;
    
    delete[] a;
    return 0;
}
