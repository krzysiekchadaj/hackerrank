#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int N, K;
    cin >> N >> K;
    
    vector<int> A(N);
    for(int n = 0; n < N; n++)
       cin >> A[n];

    int energy = 100;
    int position = 0;
    
    // first jump
    position += K;
    position %= N;
    energy--;
    if (A[position])
        energy -= 2;
    
    while (position != 0)
    {
       position += K;
       position %= N;
       energy--;
       if (A[position])
           energy -= 2;
    }
    
    cout << energy;
    return 0;
}
