#include <iostream>
#include <vector>
#include <numeric>

using namespace std;

int main()
{
    int N, K;
    cin >> N >> K;
    
    vector<int> A(N);
    for(int n = 0; n < N; n++)
        cin >> A[n];
    
    int bill;
    cin >> bill;
    
    int price = (accumulate(A.begin(), A.end(), 0) - A[K]) / 2;
    
    if ( bill == price )
        cout << "Bon Appetit";
    else
        cout << bill - price;
    
    return 0;
}
