#include <iostream>
#include <vector>
#include <algorithm>

#define LIMIT 101

using namespace std;

int main()
{
    // read inputs
    int N;
    cin >> N;
    
    vector<int> a(N);
    for(int i = 0; i < N; i++)
        cin >> a.at(i);
    
    // counting elements
    vector<int> count(LIMIT);
    
    for(int i = 0; i < LIMIT; i++)
        count.at(i) = 0;
    
    for(int i = 0; i < N; i++)
        count[a[i]] += 1;
    
    // print result
    cout << N - *max_element(count.begin(), count.end());
    
    return 0;
}
