#include <iostream>

#define LIMIT 1000000000

using namespace std;

int main()
{
    int T;
    cin >> T;

    for(int test_case = 0; test_case < T; test_case++)
    {
        int n;
        cin >> n;
        
        int number = n;
        int result = 0;
        
        int digit;
        int num = LIMIT;
        
        while(n > 0)
        {
            digit = n / num;
            if (digit != 0)
            {
                if (number % digit == 0)
                    result += 1;
                
                n -= digit * num;
            }
            num /= 10;
        }
        cout << result << endl;
    }
    return 0;
}
