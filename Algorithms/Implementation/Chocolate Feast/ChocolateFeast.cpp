#include <iostream>

using namespace std;

int main()
{
    int T;
    cin >> T;
    for(int test = 0; test < T; test++)
    {
        int n, c, m;
        cin >> n >> c >> m;
        
        int k = n / c;
        int s = k;
        int w = k;
        
        while ( w >= m )
        {
            k = w / m;
            s += k;
            w = w - k * m + k;
        }
        cout << s << endl;
    }
    return 0;
}
