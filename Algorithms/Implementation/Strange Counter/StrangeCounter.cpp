#include <iostream>

using namespace std;

#define C 3

int main()
{
    long long t;
    cin >> t;
    
    int i = 0;
    long long p = 1;
    long long u = C;
    
    while(t > u)
    {
        p *= 2;
        u = u + C * p;
        i += 1;
    }
    cout << u - t + 1;
    
    return 0;
}
