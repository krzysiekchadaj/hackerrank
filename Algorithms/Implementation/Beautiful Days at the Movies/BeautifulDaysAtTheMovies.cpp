#include <iostream>
#include <algorithm>
#include <cmath>

using namespace std;

int reversed(int x)
{
    int result = 0;
    while(x > 0)
    {
        result = result*10 + (x % 10);
        x = x / 10;
    }
    return result;
}

int main()
{
    int i, j, k;
    cin >> i >> j >> k;
    
    int result = 0;
    for(int x = i; x <= j; x++)
    {
        if ( abs(x - reversed(x)) % k == 0 )
            result++;
    }
    cout << result;
    
    return 0;
}
