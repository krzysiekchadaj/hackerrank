#include <iostream>

using namespace std;

int main()
{
    int N;
    cin >> N;

    int ** grid = new int * [N];
    for(int n = 0; n < N; n++)
        grid[n] = new int [N];
    
    char c;
    
    for(int i = 0; i < N; i++)
    {
        for(int j = 0; j < N; j++)
        {
            cin >> c;
            grid[i][j] = c - '0';
        }
    }
    
    for(int i = 0; i < N; i++)
    {
        for(int j = 0; j < N; j++)
        {    
            if ( (i != 0 && i != N - 1 && j != 0 && j != N - 1) &&
                 ( grid[i][j] > grid[i-1][j] ) && 
                 ( grid[i][j] > grid[i+1][j] ) && 
                 ( grid[i][j] > grid[i][j-1] ) && 
                 ( grid[i][j] > grid[i][j+1] ) )
                cout << 'X';
            else
                cout << grid[i][j];
        }
        cout << endl;
    }
    
    for(int n = 0; n < N; n++)
        delete[] grid[n];
    delete[] grid;
    
    return 0;
}
