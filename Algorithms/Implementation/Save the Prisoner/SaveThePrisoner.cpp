#include <iostream>

using namespace std;

int main()
{
    int T;
    cin >> T;
    
    long long N, M, S;
    for(int test = 1; test <= T; test++)
    {
        cin >> N >> M >> S;
        int result = (S + M - 1) % N;
        if (result != 0)
            cout << result << endl;
        else
            cout << N << endl;        
    }
    return 0;
}
