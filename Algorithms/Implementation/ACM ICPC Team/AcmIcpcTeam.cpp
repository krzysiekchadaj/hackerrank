#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int N, M;
    cin >> N >> M;
    
    vector< vector<bool> > topics(N, vector<bool>(M));
    char c;
    
    for(int n = 0; n < N; n++)
        for(int m = 0; m < M; m++)
        {
            cin >> c;
            topics[n][m] = c - '0';
        }
    
    int maks = 0;
    int count = 0;
    int t = 0;
    
    for(int n1 = 0; n1 < N; n1++)
        for(int n2 = n1 + 1; n2 < N; n2++)
        {
            t = 0;
            for(int m = 0; m < M; m++)
            {
                if ( topics[n1][m] == 1 || topics[n2][m] == 1 )
                    t += 1;
            }
        
            if (t == maks)
            {
                count += 1;
            }
            else if (t > maks)
            {
                maks = t;
                count = 1;
            }   
        }
    
    cout << maks << endl << count;
    
    return 0;
}
