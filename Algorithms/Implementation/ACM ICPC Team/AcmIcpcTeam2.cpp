#include <iostream>
#include <vector>
#include <bitset>

#define LIMIT 500

using namespace std;

int main()
{
    int N, M;
    cin >> N >> M;
    
    vector<string> topics(N);
    for(int n = 0; n < N; n++)
        cin >> topics.at(n);
    
    vector< bitset<LIMIT> > people(N);
    for(int n = 0; n < N; n++)
        people.at(n) = bitset<LIMIT>(topics.at(n));
       
    int maks = 0;
    int count = 0;
    int t = 0;
    
    for(int n1 = 0; n1 < N; n1++)
        for(int n2 = n1 + 1; n2 < N; n2++)
        {
            t = ( people.at(n1) | people.at(n2) ).count();
        
            if (t == maks)
            {
                count += 1;
            }
            else if (t > maks)
            {
                maks = t;
                count = 1;
            }   
        }
    
    cout << maks << endl << count;
        
    return 0;
}
