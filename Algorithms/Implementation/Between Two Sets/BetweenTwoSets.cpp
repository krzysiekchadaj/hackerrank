#include <iostream>
#include <vector>
#include <algorithm>

#define LIMIT 101

using namespace std;

int main()
{
	int N, M;
	cin >> N >> M;

	vector<int> A(N);
	for (int n = 0; n < N; n++)
		cin >> A[n];

	vector<int> B(M);
	for (int m = 0; m < M; m++)
		cin >> B[m];

	sort(A.begin(), A.end());
	sort(B.begin(), B.end());

	int result = 0;
	int isBetween;

	for (int number = 1; number < LIMIT; number++)
	{
		isBetween = true;

		for (int n = N - 1; n >= 0; n--)
		{
			if (number % A[n] != 0)
			{
				isBetween = false;
				break;
			}
		}

		if (isBetween == true)
		{
			for (int m = M - 1; m >= 0; m--)
			{
				if (B[m] % number != 0)
				{
					isBetween = false;
					break;
				}
			}
		}

		if (isBetween == true)
			result += 1;
	}

	cout << result << endl;
	return 0;
}
