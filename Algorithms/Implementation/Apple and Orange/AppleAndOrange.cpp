#include <iostream>

using namespace std;

int main(){
    int s, t, a, b, m, n, d, c;
    cin >> s >> t >> a >> b >> m >> n;
    
    c = 0;
    for(int i = 0; i < m; i++)
    {
        cin >> d;
        if( a + d >= s && a + d <= t )
            c += 1;
    }
    cout << c << endl;
    
    c = 0;
    for(int i = 0; i < n; i++)
    {
        cin >> d;
        if( b + d >= s && b + d <= t )
            c += 1;
    }
    cout << c << endl;
    
    return 0;
}
