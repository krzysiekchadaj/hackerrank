#include <iostream>

#define N 3

using namespace std;

int main()
{
    int a[N];
    for(int n = 0; n < N; n++)
        cin >> a[n];
    
    int b[N];
    for(int n = 0; n < N; n++)
        cin >> b[n];
    
    int alice = 0;
    int bob = 0;
   
    for(int n = 0; n < N; n++)
    {
        if(a[n] > b[n])
            alice += 1;
        else if (a[n] < b[n])
            bob += 1;
    }
    
    cout << alice << " " << bob;
    
    return 0;
}
