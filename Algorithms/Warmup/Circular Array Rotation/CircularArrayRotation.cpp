#include <iostream>

using namespace std;

int main()
{
    int n, k, q;
    cin >> n >> k >> q;
    
    int * b = new int[n];
    int a;
    
    for(int i = 0; i < n; i++)
        cin >> b[(i+k)%n];
    
    int m;
    for(int j = 0; j < q; j++)
    {
        cin >> m;
        cout << b[m] << endl;
    }
    
    delete b;
    return 0;
}