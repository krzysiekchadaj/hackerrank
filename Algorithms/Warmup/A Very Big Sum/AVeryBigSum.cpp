#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int n;
    cin >> n;

    vector<int> arr(n);
    for(int arr_i = 0;arr_i < n;arr_i++)
       cin >> arr[arr_i];
    
    long long int suma = 0;
    for(int i=0; i<arr.size(); i++)
        suma += arr[i];
    cout << suma;
    
    return 0;
}
