#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
    string time;
    cin >> time;
    
    string h = time.substr(0, 2);
    string m = time.substr(3, 2);
    string s = time.substr(6, 2);
    string f = time.substr(8, 2);
    string PM = "PM";
    
    int int_h =atoi(h.c_str());
    int int_m =atoi(m.c_str());
    int int_s =atoi(s.c_str());
    
    if (!f.compare(PM))
        if( int_h != 12 )
            cout << int_h+12;
        else
            cout << int_h;
    else {
        if(int_h==12)
            int_h = 0;
        if (int_h < 10)
            cout << "0";
        cout << int_h;
    }
    cout << ":" << m << ":" << s;
    
    
    return 0;
}
