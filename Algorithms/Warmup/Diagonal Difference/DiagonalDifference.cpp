#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int n;
    cin >> n;

    vector< vector<int> > a(n,vector<int>(n));
    for(int i = 0; i < n; i++)
       for(int j = 0; j < n; j++)
          cin >> a[i][j];
    
    int d1=0, d2=0;
    for (int i=0; i<n; i++)
        d1 += a[i][i];
    
    for (int i=0; i<n; i++)
        d2 += a[i][n-1-i];
    
    cout << abs(d1-d2);
    return 0;
}
