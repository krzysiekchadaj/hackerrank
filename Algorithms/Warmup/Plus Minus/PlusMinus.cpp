#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

int main()
{
    int n;
    cin >> n;

    vector<int> arr(n);
    for(int arr_i = 0;arr_i < n;arr_i++)
       cin >> arr[arr_i];
    
    int p=0, z=0, u=0;
    for(int i=0; i<n; i++)
        if (arr[i]>0)
            p++;
        else
            if (arr[i]<0)
                u++;
            else
                z++;
    
    cout << (float)p/n << endl;
    cout << (float)u/n << endl;
    cout << (float)z/n << endl;
    
        
    return 0;
}
