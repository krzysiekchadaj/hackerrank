#include <iostream>
#include <limits.h>
#include <fstream>

using namespace std;

#define UNREACHABLE -1

// A utility function to find the vertex with minimum distance value, from
// the set of vertices not yet included in shortest path tree
int minDistance(int dist[], bool sptSet[], int V)
{
	// Initialize min value
	int min = INT_MAX;
	int min_index;

	for (int v = 0; v < V; v++)
	{
		if (sptSet[v] == false && dist[v] <= min)
		{
			min = dist[v];
			min_index = v;
		}
	}

	return min_index;
}


// A utility function to print the constructed distance array
void printSolution(int * dist, int V)
{
	printf("Vertex - Distance from Source\n");
	for (int i = 0; i < V; i++)
		cout << i << " - " << dist[i] << " " << endl;
}


// Funtion that implements Dijkstra's single source shortest path algorithm
// for a graph represented using adjacency matrix representation
void dijkstra(int ** graph, int V, int source)
{
	// dist[i] will hold the shortest distance from source to i
	int * dist = new int[V];

	// sptSet[i] will true if vertex i is included in shortest
	// path tree or shortest distance from src to i is finalized
	bool * sptSet = new bool[V];

	// Initialize all distances as INFINITE
	for (int i = 0; i < V; i++)
		dist[i] = INT_MAX;

	// Initialize all stpSet as false
	for (int i = 0; i < V; i++)
		sptSet[i] = false;

	// Distance of source vertex from itself is always 0
	dist[source] = 0;

	// Find shortest path for all vertices
	for (int count = 0; count < V - 1; count++)
	{
		// Pick the minimum distance vertex from the set of vertices not
		// yet processed. u is always equal to source in first iteration.
		int u = minDistance(dist, sptSet, V);

		// Mark the picked vertex as processed
		sptSet[u] = true;

		// Update dist value of the adjacent vertices of the picked vertex.
		for (int v = 0; v < V; v++)
		{
			// Update dist[v] only if is not in sptSet, there is an edge from 
			// u to v, and total weight of path from source to  v through u is 
			// smaller than current value of dist[v]
			if ((!sptSet[v]) && (graph[u][v] != UNREACHABLE) && (dist[u] != INT_MAX) && (dist[u] + graph[u][v] < dist[v]))
			{
				dist[v] = dist[u] + graph[u][v];
			}
		}
	}

	// print the constructed distance array
	// printSolution(dist, V);
	cout << ((dist[100]==INT_MAX) ? -1 : dist[100]) << endl;

	// clean up
	delete[] sptSet;
	delete[] dist;
}

// set edges of graph
void set_edges(int ** graph, int numberOfVertices)
{
	// initiate with -1 (unreachable)
	for (int i = 0; i < numberOfVertices; i++)
		for (int j = 0; j < numberOfVertices; j++)
			graph[i][j] = UNREACHABLE;

	// set edges (dice rolls)
	for (int i = 1; i < numberOfVertices; i++)
	{
		// moves posible using a dice {1, ..., 6}
		// cost 1 means one dice roll
		if (i + 1 < numberOfVertices) graph[i][i + 1] = 1;
		if (i + 2 < numberOfVertices) graph[i][i + 2] = 1;
		if (i + 3 < numberOfVertices) graph[i][i + 3] = 1;
		if (i + 4 < numberOfVertices) graph[i][i + 4] = 1;
		if (i + 5 < numberOfVertices) graph[i][i + 5] = 1;
		if (i + 6 < numberOfVertices) graph[i][i + 6] = 1;
	}

	// set ladders
	int L, idx1, idx2;
	cin >> L;

	for (int i = 0; i < L; i++)
	{
		cin >> idx1 >> idx2;
		graph[idx1][idx2] = 0; // no roll dice
		// remove dice edges
		for (int j = 1; j <= 6; j++)
			if (idx1 + j < numberOfVertices)
				graph[idx1][idx1 + j] = UNREACHABLE;
	}

	// set snakes and remove dice edges
	// reuse: L, idx1, idx2;
	cin >> L;

	for (int i = 0; i < L; i++)
	{
		cin >> idx1 >> idx2;
		graph[idx1][idx2] = 0; // no roll dice
		// remove dice edges
		for (int j = 1; j <= 6; j++)
			if (idx1 + j < numberOfVertices)
				graph[idx1][idx1 + j] = UNREACHABLE;
	}
}


int main()
{
	// number of vertices
	int V = 101;

	// number of test cases
	int T;
	cin >> T;

	// for each test case
	for (int test_case = 0; test_case < T; test_case++)
	{
		// Allocate memory for adjoint matrix of a graph
		int ** graph = new int *[V];
		for (int i = 0; i < V; i++)
			graph[i] = new int[V];

		// set edges of the graph
		// cout << "Set edges" << endl;
		set_edges(graph, V);

		// run dijkstra algorithm from node 1
		// cout << "Run Dijkstra" << endl;
		dijkstra(graph, V, 1);

		// clean up
		for (int i = 0; i < V; i++)
			delete[] graph[i];
		delete[] graph;
	}
	return 0;
}
