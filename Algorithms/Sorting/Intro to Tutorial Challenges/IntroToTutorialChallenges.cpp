#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main()
{
    int V, N, a;
    
    cin >> V >> N;
    
    for(int n = 0; n < N; n++)
    {
        cin >> a;
        if (a == V)
        {
            cout << n;
            return 0;
        }
    }

    return 0;
}
