#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int T;
    cin >> T;
    
    for(int test_case = 0; test_case < T; test_case++)
    {
        int N;
        cin >> N;
        
        vector<int> price(N);
        for(int i = 0; i < N; i++)
            cin >> price.at(i);
        
        // look for maximum price value from the end to beginning
        // if maximum price value is bigger than actual price value,
        // it is worth to buy a price and sell it later at maximum value price
        // i.e. for test case 3:
        // day:              1 2 3 4 
        // price:            1 3 1 2
        // max backwards:    3 3 2 2
        // predicted profit: 2 0 1 0
        // profit sum:       3 1 1 0
        int max = 0;
        long profit = 0;
        for(int i = N-1; i > -1; i--)
        {
            if (max < price[i])
                max = price[i];
            profit += ( max - price[i] );
        }
        cout << profit << endl;
    }
    return 0;
}
