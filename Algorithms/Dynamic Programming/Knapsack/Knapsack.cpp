#include <iostream>

using namespace std;

int main()
{
    int T, N, K;
    cin >> T;
    for(int test_case = 0; test_case < T; test_case++)
    {
        cin >> N >> K;
        
        // numbers
        int * number = new int[N];
        for(int n = 0; n < N; n++)
            cin >> number[n];
        
        // allocate memory for values
        int * value = new int[K+1];
        
        // initialize
        for(int v = 0; v <= K; v++)
            value[v] = 0;
        
        // fill for other values
        for(int v = 1; v <= K; v++)
            for(int n = 0; n < N; n++)
                if( (v-number[n] >= 0) // correct index condition
                    && (value[v-number[n]] + number[n] <= v) // we do not want to exceed considered value v
                    && (value[v-number[n]] + number[n] > value[v]) ) // is result using number n better?
                    value[v] = value[v-number[n]] + number[n];
            
        // print result
        cout << value[K] << endl;
        
        // clean up
        delete[] number;
        delete[] value;
    }
   
    return 0;
}
