#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <numeric>

using namespace std;

void print(vector<int> v) {
    cout << "Vector<int>: ";
    for(unsigned int i=0; i<v.size(); i++)
       cout << v[i] << " ";
    cout << endl;
}
   
int main() {

    int T, N;
    int temp;
    int suma;
    int maks;
    int max_end, max_slice;
    
    cin >> T;
    vector<int> a;
    vector<int> b;
    //cout << " T = " << T << endl;
        
    for(int test=0; test<T; test++) {
        
        cin >> N;
        for(int i=0; i<N; i++) {
            cin >> temp;
            a.push_back(temp);
        }
        //print(a);
        
        // Make copy
        b = a;
        //print(b);
        
        suma = 0;
        for(unsigned int i = 0; i < a.size(); i++)
            if (a[i] >= 0)
                suma += 1;
        if (suma == 0) {
            maks = a[0];
            for(unsigned int i = 1; i < a.size(); i++)
                if (a[i] > maks)
                    maks = a[i];
            
            cout << maks << " " << maks << endl;
            continue;    
        }
            
        
        
        // Compute cont sum
        max_end = 0;
        max_slice = 0;
        for(unsigned int i = 0; i < a.size(); i++) {
            max_end = max(0, max_end + a[i]);
            max_slice = max(max_slice, max_end);
        }
        cout << max_slice << " ";
        
        // Compute non cont sum
        suma = 0;
        for(unsigned int i = 0; i < b.size(); i++)
            if (b[i] > 0)
                suma += b[i];
        cout << suma << endl;
        
        // Clean up
        a.clear();
        b.clear();
    }
        
    return 0;
}
