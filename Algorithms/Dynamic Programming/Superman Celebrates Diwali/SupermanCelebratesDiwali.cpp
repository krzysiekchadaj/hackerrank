#include <iostream>
#include <iomanip>
#include <fstream>

#define DEBUG 0

using namespace std;

// initialize 1D array
void init1D(int * tab, int size)
{
	for (int r = 0; r < size; r++)
		tab[r] = 0;
}

// initialize 2D matrix
void init2D(int ** tab, int rows, int cols)
{
	for (int r = 0; r < rows; r++)
		for (int c = 0; c < cols; c++)
			tab[r][c] = 0;
}

// compute maximum of two numbers
int max(int a, int b)
{
	return (a > b ? a : b);
}

// print state
void print(int ** people, int * maxOnFloor, int H, int N)
{
	if (DEBUG)
	{
		for (int h = 0; h < H; h++)
		{
			cout << setw(2) << h << ":   ";
			for (int n = 0; n < N; n++)
				cout << people[h][n] << " ";
			cout << "   -   " << maxOnFloor[h] << endl;
		}
		cout << endl;
	}
}

int main()
{
	//fstream cin("input0.txt", ios::in);

	int N;	// number of buildings
	int H;	// height of buildings
	int I;	// height loose with jump to another building
	cin >> N >> H >> I;
	if(DEBUG)
		cout << "N = " << N << ", H = " << H << ", I = " << I << endl << endl;

	int n, h, u; // iterators

	// allocate memory for people in buildings: people[floor][building]
	int ** people = new int * [H];
	for (h = 0; h < H; h++)
		people[h] = new int[N];
	init2D(people, H, N);

	// read inputs
	int U;	// number of people in building
	int t;	// temporary variable
	for (n = 0; n < N; n++)
	{
		cin >> U;
		for (u = 0; u < U; u++)
		{
			cin >> t;
			people[H-t][n] += 1;
		}
	}

	//cin.close();

	// allocate memory for sub result array
	int * maxOnFloor = new int[H];
	init1D(maxOnFloor, H);
	
	// print state
	print(people, maxOnFloor, H, N);

	// loop over all floors
	for (h = 0; h < H; h++)
	{
		// loop over all buildings
		for (n = 0; n < N; n++)
		{
			// if jump possible
			if (h > I - 1)
			{
				// make jump or climb down one floor
				people[h][n] += max(people[h - 1][n], maxOnFloor[h - I]);
			}
			else if (h > 0)
			{
				// if jump not possible, check if possible to climb down and do it
				people[h][n] += people[h - 1][n];
			}

			if (maxOnFloor[h] < people[h][n])
				maxOnFloor[h] = people[h][n];
		}
		print(people, maxOnFloor, H, N);
	}
	cout << maxOnFloor[h - 1];
	cout << endl;

	// clean up
	delete[] maxOnFloor;

	for (int h = 0; h < H; h++)
		delete[] people[h];
	delete[] people;

	return 0;
}
