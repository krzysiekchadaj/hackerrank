#include <iostream>

using namespace std;

void count(int * coin, int M, int N)
{
	// We need n+1 rows as the table is consturcted in bottom up manner using 
	// the base case 0 value case (n = 0)
	long long ** result = new long long *[N+1];
	for (int n = 0; n < N+1; n++)
		result[n] = new long long[M];

	// Fill the enteries for 0 value case (n = 0)
	for (int m = 0; m < M; m++)
		result[0][m] = 1;

	// Fill rest of the table enteries in bottom up manner  
	long long x, y;
	for (int n = 1; n < N + 1; n++)
	{
		for (int m = 0; m < M; m++)
		{
			// Count of solutions including S[j]
			x = (n - coin[m] >= 0) ? result[n - coin[m]][m] : 0;

			// Count of solutions excluding S[j]
			y = (m >= 1) ? result[n][m - 1] : 0;

			// total count
			result[n][m] = x + y;
		}
	}

	cout << result[N][M - 1];

	// clean up
	for (int n = 0; n < N+1; n++)
		delete[] result[n];
	delete[] result;
}

int main()
{
	int N; // amount
	int M; // number of coins
	cin >> N >> M;

	int * coin = new int[M];
	for (int m = 0; m < M; m++)
		cin >> coin[m];

	// solve
	count(coin, M, N);
	
	// clean up
	delete[] coin;
	return 0;
}
