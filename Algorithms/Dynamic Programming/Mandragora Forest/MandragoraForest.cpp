#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>

using namespace std;

int main()
{
    int T;
    cin >> T;
    
    for(int test_case = 0; test_case < T; test_case++)
    {
        int N;
        cin >> N;
        
        vector<long long> H(N);
        for(int i = 0; i < N; i++)
            cin >> H.at(i);
        
        // as eating increments strength by one point regardles what mandragora is eaten
        // the most benefitial meal would be the mandragora with lowest health points
        // border cases:
        // * eat all mandragoras:    S = N + 1, P = 0
        // * battle all mandragoras: S = 1,     P = 1 * sum(H)
        // optimal solution is somewhere in the middle: eat i smallest mandragoras, fight the rest
        // the expirience would be: P = S * ( sum of the rest of H )
        // where: S = i + 1
        //        sum of the rest of H = H_(i+1) + H_(i+2) + ... + H_(N-1) [cummulative sum]
        
        // sort mandragoras
        sort(H.begin(), H.end());
        
        // loop over number of eaten mandragoras to find out optimal solution
        int S = 1;
        long long sum = accumulate(H.begin(), H.end(), (long long)0);
        
        long long P = S * sum;
        long long prevP = 0;
        int i = 0;

        while ( P > prevP )
        {
            prevP = P;
            S++;
            sum -= H.at(i);
            P = S * sum;
            i++;
        }
        cout << prevP << endl;
    }
    return 0;
}
