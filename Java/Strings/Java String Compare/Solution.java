import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);
        String text = scanner.next();
        int k = scanner.nextInt();
        scanner.close();
                  
        String min = "z";
        String max = "A";
        String substring;
        
        for(int i = 0; i < text.length() - k + 1; i++) {
            substring = text.substring(i, i + k);
            
            if (substring.compareTo(min) < 0) {
                min = substring;
            }
            
            if (substring.compareTo(max) > 0) {
                max = substring;
            }
        }
        
        System.out.println(min);
        System.out.println(max);
    }
}