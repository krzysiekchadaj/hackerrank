import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);
        String word = scanner.next();
        scanner.close();
        
        int wordLength = word.length();
        String leftLetter;
        String rightLetter;
        
        boolean isPalindrome = true;
        for(int i = 0; i < wordLength / 2; i++) {
            leftLetter = word.substring(i, i + 1);
            rightLetter = word.substring(wordLength - 1 - i, wordLength - i);
            if (!leftLetter.equals(rightLetter)) {
                System.out.println("No");
                return;
            }
        }
        System.out.println("Yes");
    }
}
