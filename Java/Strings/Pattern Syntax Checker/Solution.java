import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class Solution
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        int testCases = Integer.parseInt(in.nextLine());
        boolean isValid;
        
        for (int test = 0; test < testCases; test++) {
            String pattern = in.nextLine();
            isValid = true;
            
            try {
                Pattern.compile(pattern);
            } catch(PatternSyntaxException e) {
                isValid = false;
            }
            
            if (isValid) {
                System.out.println("Valid");
            } else {
                System.out.println("Invalid");
            }
        }
        in.close();
    }
}
