import java.util.Scanner;

class Solution {
    public static void main(String[] argh) {
        Scanner in = new Scanner(System.in);
        int numberOfTests = in.nextInt();
        
        for(int testNumber = 0; testNumber < numberOfTests; testNumber++) {
            int a = in.nextInt();
            int b = in.nextInt();
            int n = in.nextInt();
            
            int q = a;
            for(int i = 0; i < n; i++) {
                q += ( Math.pow(2, i) * b );
                System.out.print(q + " ");
            }
            System.out.println();
        }
        in.close();
    }
}
