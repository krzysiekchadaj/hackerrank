import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    static boolean flag = true;
    static int B = 0;
    static int H = 0;
    static int area = 0;

    static {
        try {
            Scanner scanner = new Scanner(System.in);
            B = scanner.nextInt();
            H = scanner.nextInt();
            scanner.close();
            area = B * H;
            if (B <= 0 || H <= 0) {
                throw new Exception("Breadth and height must be positive");
            }
        } catch (Exception e) {
            flag = false;
            System.out.println(e.toString());
        }
    }
    
    public static void main(String[] args) {
		if (flag) {
			int area = B * H;
			System.out.print(area);
		}
	} //end of main

} //end of class
