import java.util.Date;
import java.util.Locale;
import java.util.Scanner;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String month = in.next();
        String day = in.next();
        String year = in.next();
        in.close();
        
        int monthInt = Integer.parseInt(month);
        int dayInt = Integer.parseInt(day);
        int yearInt = Integer.parseInt(year);
        
        try {
            String dateString = String.format("%d-%d-%d", yearInt, monthInt, dayInt);
            Date date = new SimpleDateFormat("yyyy-M-d").parse(dateString);
            String dayOfWeek = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date);
            System.out.println(dayOfWeek.toUpperCase());
        } catch (ParseException e) {
            System.out.println(e.toString());
        }
    }
}
